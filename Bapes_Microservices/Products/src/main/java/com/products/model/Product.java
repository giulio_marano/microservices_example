package com.products.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//////////////////////////////////
//// MODEL - PRODUCT /////////////
//////////////////////////////////
@Entity
public class Product {

//This Microservice manages the product. It proposes 2 simple operations: list all the products and
//retrieve a product by its id. It listens to port 9001

    //Params for relational tabs, with id as main.
    @Id
    @GeneratedValue
    private int id;
    private String title;
    private String description;
    private String image;
    private Double price;


    //Constructors
    public Product() {
    }

    public Product(int id, String title, String description, String image, Double price){
        this.id=id;
        this.title=title;
        this.description=description;
        this.image=image;
        this.price=price;

    }

    //Getters & Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    //toString method
    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", price=" + price +
                '}';
    }
}
