package com.products.dao;

import com.products.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//////////////////////////////////
////// DAO - PRODUCT /////////////
//////////////////////////////////

@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {
}
