package com.products.web.controller;


import com.products.dao.ProductDao;
import com.products.model.Product;
import com.products.web.exceptions.ProductNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    final ProductDao product;

    public ProductController(ProductDao product) {
        this.product = product;
    }

    @GetMapping(value = "/products")
    public List<Product> productList(){
        List<Product> productList= product.findAll();
        if(productList.isEmpty()) throw  new ProductNotFoundException("Error 404: Product not found");

        return productList;

    }

    @GetMapping(value = "/products/{id}")
    public Optional<Product> retrieveAProduct(@PathVariable int id){

        //Optional allows to stop checking if the object is null each time and avoids NullPointerExceptions. You
        //can read more here.
        Optional<Product> productId= product.findById(id);

        if(!productId.isPresent()) throw new ProductNotFoundException("Error 404: Product not found");

        return productId;
    }
}
