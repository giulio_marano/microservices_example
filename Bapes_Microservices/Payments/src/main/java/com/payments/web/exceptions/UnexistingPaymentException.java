package com.payments.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.ALREADY_REPORTED)
public class UnexistingPaymentException extends RuntimeException{

    public UnexistingPaymentException(String message) {
        super(message);
    }

}
