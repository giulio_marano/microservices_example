package com.payments.web.controller;


import com.payments.dao.PaymentDao;
import com.payments.model.Payment;
import com.payments.web.exceptions.ImpossiblePaymentException;
import com.payments.web.exceptions.UnexistingPaymentException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class PaymentController {

    final PaymentDao paymentDao;

    public PaymentController(PaymentDao paymentDao) {
        this.paymentDao = paymentDao;
    }

    @PostMapping(value = "/payment")
    public ResponseEntity<Payment> payOrder(@RequestBody Payment payment){

        Optional<Payment> existingPayment = paymentDao.findById(payment.getOrderId());
        if(existingPayment != null) throw new UnexistingPaymentException("This payment already exist");


        Payment newPayment = paymentDao.save(payment);


        if(newPayment == null) throw new ImpossiblePaymentException("Error, we encountered some problem with your payment.");


        return new ResponseEntity<Payment>(newPayment, HttpStatus.CREATED);

    }

}
