package com.payments.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ImpossiblePaymentException extends RuntimeException {

    public ImpossiblePaymentException(String string){super(string);}

}
