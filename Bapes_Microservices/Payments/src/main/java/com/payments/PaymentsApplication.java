package com.payments;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
@SpringBootApplication
public class PaymentsApplication {

    public static void main(String[] args){ SpringApplication.run(PaymentsApplication.class, args);}
}
