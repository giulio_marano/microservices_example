package com.payments.dao;


import com.payments.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//////////////////////////////////
///// DAO - PAYMENT //////////////
//////////////////////////////////

@Repository
public interface PaymentDao extends JpaRepository<Payment, Integer> {
}
