package com.payments.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//////////////////////////////////
//// MODEL - PAYMENT /////////////
//////////////////////////////////

@Entity
public class Payment {

    @Id
    @GeneratedValue
    private int id;
    private Integer orderId;
    private Integer amount;
    private Long cardNumber;


    public Payment() {
    }

    public Payment(int id, Integer orderId, Integer amount, Long cardNumber) {
        this.id = id;
        this.orderId = orderId;
        this.amount = amount;
        this.cardNumber = cardNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }


    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", amount=" + amount +
                ", cardNumber=" + cardNumber +
                '}';
    }
}
