package com.orders.dao;

import com.orders.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//////////////////////////////////
///// DAO - ORDER ////////////////
//////////////////////////////////

@Repository
public interface OrderDao extends JpaRepository<Order, Integer> {
}
