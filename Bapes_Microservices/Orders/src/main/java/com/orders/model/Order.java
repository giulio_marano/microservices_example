package com.orders.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

//////////////////////////////////
//// MODEL - ORDER ///////////////
//////////////////////////////////

@Entity
public class Order {

    @Id
    @GeneratedValue
    private int id;
    private Integer productId;
    private Date orderDate;
    private Integer quantity;
    private Boolean paidOrder;

    public Order() {}

    public Order(int id, Integer productId, Date orderDate, Integer quantity, Boolean paidOrder) {
        this.id = id;
        this.productId = productId;
        this.orderDate = orderDate;
        this.quantity = quantity;
        this.paidOrder = paidOrder;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getPaidOrder() {
        return paidOrder;
    }

    public void setPaidOrder(Boolean paidOrder) {
        this.paidOrder = paidOrder;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", productId=" + productId +
                ", orderDate=" + orderDate +
                ", quantity=" + quantity +
                ", paidOrder=" + paidOrder +
                '}';
    }
}
