package com.orders.web.controller;


import com.orders.dao.OrderDao;
import com.orders.model.Order;
import com.orders.web.exceptions.CannotAddOrderException;
import com.orders.web.exceptions.OrderNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class OrderController {

    final OrderDao orderDao;

    public OrderController(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @PostMapping(value = "/order")
    public ResponseEntity<Order> AddOrder(@RequestBody Order order){
        Order newOrder = orderDao.save(order);
        if(newOrder == null) throw  new CannotAddOrderException("Error 500: command cannot be registered due to an internal error");

        return new ResponseEntity<>(order, HttpStatus.CREATED);

    }

    @GetMapping(value = "/order/{id}")
    public Optional<Order> GetOrder(@PathVariable int id){

        Optional<Order> order= orderDao.findById(id);

        if(!order.isPresent()) throw new OrderNotFoundException("Error 404: Product not found");

        return order;
    }


}
