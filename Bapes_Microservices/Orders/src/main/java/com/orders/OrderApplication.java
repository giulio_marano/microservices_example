package com.orders;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
@SpringBootApplication
public class OrderApplication {

    public static void main(String[] args){ SpringApplication.run(OrderApplication.class, args);}
}
